import C from "../Constants";

export const addNote = (id, newtext) => {
 //   debugger;
    return {
        type: C.ADD_NOTE,
        payload: {
            id: id,
            note: newtext
        }
    }
}

export const editNote = (id, newtext) => (dispatch,getState) => {
   // debugger;
    const EditedNotesArray = getState().noteReducer.notes.map(
                             eachnote => (eachnote.id !==id) ? eachnote
                             : {...eachnote,note:newtext})
    dispatch({
        type: C.EDIT_NOTE,
        payload: EditedNotesArray
    })
}
export const deleteNote = (id) => (dispatch,getState) =>{
    debugger;
    const filteredNotesArray = getState().noteReducer.notes.filter(item=> item.id !==id)
    dispatch({
        type:C.DELETE_NOTE,
        payload: filteredNotesArray
    })
}

export const toggleDisplayNote = (shownote) =>{
   if(shownote)
    return {
        type:C.SHOW_NOTE,
        payload:true
    }
    else{
        return{
            type: C.HIDE_NOTE,
            payload:false
        }
    }
}

export const cachedNotes = (cacheddata) => {
    debugger;
    return {
        type:C.CACHED_NOTE,
        payload:cacheddata
    }
}
 