import React, { Component } from 'react';
import {BrowserRouter,Route,Switch,Link } from "react-router-dom";
import './App.css';
import Note from "./Components/Note";
import AppReducer from "./Reducers/Notereducer";
import { createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';

const store = createStore(AppReducer, applyMiddleware(thunk))

const Home = () =>{
  return (
    <div>
      <h1>
        Note App
      </h1>
      <Link to="/note">
        <button className="button"> 
          NoteUI
        </button>
      </Link>
    </div>
  )
}
class App extends Component {
  render() {
    return (
      <div className="App">
      <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route path="/" exact component={Home}/>
              <Route path="/0" component={Home}/>
              <Route path="/note" component={Note}/>
            </Switch>
          </BrowserRouter>
        </Provider>
      </div>
    );
  }
}

export default App;

//<Route path="/edit/:id" component={EditNote}/>
