import React, {Component} from 'react';
import uniqid from 'uniqid';
import * as Actions from "../Actions/actions";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class AddNewNote extends Component{
    constructor(props){
        super(props)
        this.state= {
            newnote:false
        }
this.fn_local_AddNote = this.fn_local_AddNote.bind(this)
this.fn_new_note=this.fn_new_note.bind(this)
}
componentDidUpdate(){
    console.log('store state as props',this.props.notes)
}
componentWillReceiveProps(nextProps) {
    console.log('#componentWillReceiveProps - AddNewNote#')
    console.log('AddNewNote State here', this.state)
    console.log('AddNewNote Props here', this.props)
    console.log('AddNewNote nextProps here', nextProps)
}
fn_local_AddNote(e){
    let id = uniqid('note-')
    //console.log('id', uniqid('note-'))
    //let id = Math.floor(Math.random() * 1000)
    let temp_text = this.noteval.value
    //console.log('this.state.noteval', this.state.noteval)
    console.log('fn_local_AddNote->id', id)
    console.log('fn_local_AddNote->temp_text->', temp_text)
    e.preventDefault() // stops from reloading page
    if (temp_text !== '' ) {
        this.props.addNote(id,temp_text)
    }
    this.noteval.value = null
    this.setState({
        newnote: !this.state.newnote
    }, () => {
        console.log('fn_local_AddNote->Changed State', this.state.newnote)
    })
}
fn_new_note(){
    console.log('fn_new_note->Setting state newnote to true')
    this.setState({
        newnote:!this.state.newnote
    },()=>{
        console.log('fn_new_note->Changed State',this.state.newnote)
    })
}
render(){      
        return (
                <div>
                    <button className="button"
                            onClick={this.fn_new_note}>Add New Note
                    </button>
                        { this.state.newnote ?   
                            <form onSubmit={this.fn_local_AddNote}>
                                <div>
                                    <input  id="Note" 
                                            ref={input =>this.noteval = input}
                                            type='text' 
                                            placeholder="Enter here"/>
                                    <span> </span>
                                    <button className="button"
                                            onClick={this.fn_local_AddNote}>Save Note
                                    </button>     
                                </div>
                            </form>
                            :null
                        }
                </div>
        )
    }
}

const mapStateToProps = (state) =>{
   return {
            notes: state.noteReducer.notes
    }
}

const mapDispatchToProps = (dispatch)=> {
    return bindActionCreators(Actions,dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewNote)