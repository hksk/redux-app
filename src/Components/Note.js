import React, {Component} from 'react';
import { Link } from "react-router-dom";
import '../App.css';
import AddNewNote  from "./AddNewNote";
import Notes from "./NoteSub";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "../Actions/actions";

class Note extends Component{
    constructor(props){
        super(props)
        this.state = {
            buttontxt:'Show Notes'
        }    
        this.fn_change_button_text = this.fn_change_button_text.bind(this)
}
// componentDidMount(){
//     console.log('componentDidMount ->', this.props)
//     const {cachedNotes} =this.props
//     const cachedstate = sessionStorage.getItem('Current Store State')
//     const parsedstate = JSON.parse(cachedstate)
//     console.log('parsedstate->',parsedstate)
//     if (cachedstate){
//         debugger;
//         cachedNotes(parsedstate)
//     }
// }
// componentWillUnmount() {
//     debugger;
//     console.log('componentWillUnmount ->', this.props)
//     const tobechached ={notes:this.props.notes,showNote:this.props.showNote}
//     sessionStorage.setItem('Current Store State', JSON.stringify(tobechached))
// }
fn_change_button_text(){
    (this.props.showNote) ? 
   this.setState({
       buttontxt: 'Show Notes'
   }): this.setState({
       buttontxt: 'Hide Notes'
   })  
}
    render(){
        return(
            <div className="App">
                <h1>Notes</h1>
                <AddNewNote AddNote={this.fn_main_add_note}/>
                <br/>
                <button className="button" 
                        onClick ={(event)=>{(this.props.toggleDisplayNote(!this.props.showNote));this.fn_change_button_text()}}>
                    {this.state.buttontxt}    
                </button>        
                <br/> <br/>
                <Link to="/0">
                    <button className="button">
                        Home
                    </button>
                </Link>
                <br/> <br/>    
                {
                    this.props.showNote ?
                        <Notes notes={this.props.notes}
                               showstat={this.props.showNote}
                        /> 
                    : null
                }
                <br/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        showNote: state.noteReducer.showNote,
        notes: state.noteReducer.notes
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Actions,dispatch)
}
export default connect (mapStateToProps,mapDispatchToProps) (Note);