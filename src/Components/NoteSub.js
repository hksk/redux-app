import React, {Component} from 'react';
import '../App.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "../Actions/actions";

class Notes extends Component {
    constructor(props){
        super(props);
        console.log('constructor->this.props.notes-Notes', this.props.notes)
        console.log('constructor->this.props.notes-showstat', this.props.showstat)
        this.state = {
            editing:false,
            current_item:null
        }
        this.fn_local_delete_note = this.fn_local_delete_note.bind(this)
        this.fn_local_edit_note = this.fn_local_edit_note.bind(this)
        this.fn_handle_change=this.fn_handle_change.bind(this)
        this.fn_edit_note=this.fn_edit_note.bind(this)
        this.fn_save_change=this.fn_save_change.bind(this)
}
componentWillReceiveProps(newprops){
    console.log('#componentWillReceiveProps - Notessss#')
    console.log('State here',this.state)
    console.log('Old Props here', this.props)
    console.log('New Props here', newprops)
}
// componentWillMount() {
//     console.log('#componentWillMount - Notessss#')
// }
// componentDidMount(){
//     console.log('#componentDidMount - Notessss#')
// }
// shouldComponentUpdate(nextProps, nextState) {
//     console.log("#shouldComponentUpdate - Notessss#")
//     //return (this.props.children !== nextProps.children || this.state !== nextState)
//     return true
// }
// componentWillUpdate(){
//     console.log("#componentWillUpdate - Notessss#")
//     return true
// }
// componentDidUpdate() {
//     console.log("#componentDidUpdate - Notessss#")
//     return true
// }
fn_local_delete_note(id){
    console.log('fn_local_delete_note->removing item ', id)
    console.log('fn_local_delete_note->calling deleteNote from Actions')
    this.props.deleteNote(id) // goto Note.js
    console.log('fn_local_delete_note->current Notes from NoteSub..', this.state.notes)
}
fn_local_edit_note(id,newtext) {
    console.log('fn_local_edit_note->editing item ', id)
    console.log('fn_local_edit_note->calling onMainEdit from Main Note Comp')
   // e.preventDefault();
    this.props.editNote(id, newtext) // goto Note.js
    this.setState({
        editing: !this.state.editing
    })
}
fn_edit_note(id){
    console.log('fn_edit_note->Editing now...')
    console.log('fn_edit_note->Got item id->', id)
       this.setState({
        editing: !this.state.editing,
        current_item:id
       }, () => {
        console.log('fn_edit_note->After fn_edit_note ->', this.state.editing)
    })
}
fn_handle_change(id,e){
    console.log('fn_handle_change->Item is edited.so rerender note with', id, ' to new value')
    console.log('fn_handle_change->e.target.value->', e.target.value)
    // this.setState({
    //     editing: !this.state.editing
    // }, () => {
    //     console.log('After fn_handle_change ->', this.state.editing)
    // })
    this.props.editNote(id, e.target.value) // Always call Main Component For State changes related to rendering comps
}
fn_save_change(){
    console.log('fn_save_change->Saving now..not messing with state')
    this.setState({
        editing: !this.state.editing
    }, () => {
        console.log('fn_save_change->After fn_save_change ->', this.state.editing)
    })
}
    render(){
        if (this.props.notes.length) {
                return(
                    <div>
                        {
                            this.props.notes.map((item,index) => {
                                return(
                                        <div key={index}>
                                        <ShowNotes  notes={item} 
                                                    cur_show_stat={this.props.showstat}
                                                    cur_item ={item} 
                                                    onDelete={this.fn_local_delete_note}
                                                    onEdit={this.fn_edit_note}
                                                    editing={this.state.editing}
                                                    handleChange={this.fn_handle_change}
                                                    saveChange ={this.fn_save_change}
                                                    current_item_id={this.state.current_item}                                                 
                                        />            
                                        <br/>
                                        </div>
                                    )
                                }
                            )
                        }
                    </div>
            )                                       
        } 
        else{
             return null;
        }
    }
}
const ShowNotes = ({notes,cur_show_stat,cur_item,onDelete,onEdit,editing,handleChange,saveChange,current_item_id}) =>{
    console.log('ShowNotes->In stateless ShowNotes comp')
    console.log('ShowNotes->cur_item', cur_item)
    console.log('ShowNotes->current_item_id->', current_item_id)
    console.log('ShowNotes->cur_show_stat->', cur_show_stat)
    return (cur_show_stat && (notes.id !== '')) ? 
            <div>
                <div className = "textdiv" >
                        {notes.note}      
                </div>
                <br/> 
                    <button className="button" 
                            onClick={() => onDelete(cur_item.id)}>
                    Delete Note 
                    </button>
                    <span>  </span>
                    <button className = "button"
                            onClick={()=>onEdit(cur_item.id)}>  
                    Edit Note
                    </button>
                        <EditNote   note={notes.note}
                                    cur_item={cur_item}
                                    editing={editing} 
                                    handleChange={handleChange}
                                    saveChange={saveChange}
                                    current_item_id={current_item_id}
                        /> 
            </div > : null
}
const EditNote = ({note,cur_item,editing,handleChange,saveChange,current_item_id}) => {
    console.log('EditNote-> In EditNote')
    console.log('EditNote-> note', note)
    console.log('EditNote-> editing', editing)
    console.log('EditNote-> cur_item', cur_item)
    console.log('EditNote-> current_item_id->', current_item_id)
   if (editing && (current_item_id === cur_item.id)) {
       console.log('note---->', note)
        return (
                <div>
                    <br/>
                        <textarea   value={note}
                                    id={cur_item.id}
                                    onChange ={(e)=>handleChange(cur_item.id,e)}
                        />
                    <br/>
                    <button className = "button"
                            onClick ={saveChange}>     
                    Save Note
                    </button>
                </div>
        )
    } else
        {
            return null 
    }
}

const mapStateToProps = (state) =>{
    return{
        notes : state.noteReducer.notes,
        shownote: state.noteReducer.shownote,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Actions,dispatch)
}

export default connect (mapStateToProps,mapDispatchToProps) (Notes)