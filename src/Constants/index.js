const constants = {
    ADD_NOTE: "ADD_NOTE",
    SHOW_NOTE: "SHOW_NOTE",
    HIDE_NOTE: "HIDE_NOTE",
    DELETE_NOTE: "DELETE_NOTE",
    EDIT_NOTE: "EDIT_NOTE",
    CACHED_NOTE: "CACHED_NOTE"
}
export default constants;