import C from "../Constants";
import { combineReducers } from "redux";

let initialState={
    showNote: false,
    notes: []
}
const noteReducer = (state=initialState, action) => {
    //debugger;
    console.log('Notereducer->state', state)
    console.log('Notereducer->action', action);
    switch (action.type) {
        case C.ADD_NOTE:
   //     debugger;
            return {...state,
                        notes:[...state.notes,action.payload]
                }
        case C.SHOW_NOTE:
            return {
                    ...state,
                    showNote:action.payload
                }
        case C.HIDE_NOTE:
            return {
                    ...state,
                    showNote:action.payload
                }
        case C.EDIT_NOTE:
      //  debugger;
            return {
                ...state,
                notes:action.payload
            }    
        case C.DELETE_NOTE:
            debugger;
            return {
                ...state,
                notes:action.payload
            }
        case C.CACHED_NOTE:
        debugger;
       // const tempstate=state
            return {
                
            }
        default:
            return state    
    }    
}

export default combineReducers({
    noteReducer
})
